import { createRouter, createWebHistory } from 'vue-router'
import MainLayout from "./layouts/mainLayout.vue";
export default createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            component: MainLayout,
            redirect: '/',
            children: [
                {
                    path: '/',
                    component: () => import('./pages/MyTasks.vue')
                },
                {
                    path: '/myTasks',
                    component: () => import('./pages/MyTasks.vue')
                },
                {
                    path: '/complete_tasks',
                    component: () => import('./pages/CompleteTasks.vue'),
                },
                {
                    path: '/deleted_tasks',
                    component: () => import('./pages/DeletedTasks.vue'),
                },
                {
                    path: '/important_tasks',
                    component: () => import('./pages/ImportantTasks.vue'),
                },
                {
                    path: '/search',
                    component: () => import('./pages/SearchPage.vue'),
                },
            ]

        },
    ],
})
