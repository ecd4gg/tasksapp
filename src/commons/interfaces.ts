export interface ITask {
    id: string
    title: string
    description: string
    important: boolean
    endDate: string
    endDateOpt?: string
    tags?: string[]
    deleted: boolean
    complete: boolean
    matchWord: string
}

export interface IModalProps {
    title: string;
    modalClass: string;
}
export interface ITags {
    productivity: boolean
    education: boolean
    health: boolean
    urgent: boolean
}