export enum Tags {
    education = 'education',
    health = 'health',
    urgent = 'urgent',
    productivity = 'productivity'
}