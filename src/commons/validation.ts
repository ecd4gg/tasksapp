import {reactive} from "vue";

export const errors = reactive({
    title: '',
    endDate: ''
});

export const validateField = (data: any, field: string) => {
    switch (field) {
        case 'title':
            errors.title = data.title ? '' : 'Название задачи обязательно';
            break;
        case 'endDate':
            errors.endDate = data.endDate ? '' : 'Дата завершения задачи обязательна';
            break;
        default:
            break;
    }
};

export const validateForm = (data: any) => {
    validateField(data, 'title');
    validateField(data, 'endDate');
};

export const isFormValid = () => {
    return !errors.title && !errors.endDate;
};
