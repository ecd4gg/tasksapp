import {DateTime} from "ts-luxon";

export const getPageTitle = (route: string, focused: boolean): string => {
    const routeName = route;
    let title: string;
    if (focused) title = 'Результаты поиска';
    switch (routeName) {
        case "/complete_tasks":
            title = "Выполненные"
            break;
        case "/deleted_tasks":
            title = "Удаленные"
            break;
        case "/important_tasks":
            title = "Важные"
            break;
        case "/myTasks" || '/':
            title = "Мои Задачи"
            break;
        case "/search":
            title = "Результаты поиска"
            break;
        default:
            title = "Приложение Задач"
            break;
    }
    return title;
}
export const boldPartOfString = (charToBolt: string, text: string): string => {
    const regex = new RegExp(`(${charToBolt})`, 'gi'); // Create a case-insensitive regex for the search inputText
    return text.replace(regex, '');
};

export const formatEndDate = (endDate: string) => {
    return DateTime.fromISO(endDate).toFormat('HH:mm / dd.MM.yyyy');
};

export const highlightMatch = (text: string, query: string): string => {
    const regex = new RegExp(`(${query})`, 'gi');
    return text.replace(regex, '<b>$1</b>');
}