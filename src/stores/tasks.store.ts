import {defineStore} from 'pinia';
import {Tags} from "../commons/dataSets/tags.ts";
import {ITask} from "../commons/interfaces.ts";

export const useTaskStore = defineStore('taskStore', {
    state: () => ({
        tasks: [
            {
                id: '1',
                title: 'Task1',
                description: 'Some description',
                endDate: "11:09 / 09.09.2022",
                important: false,
                tags: [Tags.education, Tags.productivity],
                complete: true,
                deleted: false,
                matchWord: ''
            },
            {
                id: '2',
                title: 'Task2',
                description: 'Some description',
                endDate: "11:09 / 09.09.2022",
                important: false,
                tags: [Tags.urgent, Tags.health],
                complete: true,
                deleted: false,
                matchWord: ''
            },
            {
                id: '3',
                title: 'Task3',
                description: 'Some description',
                endDate: "11:09 / 09.09.2022",
                important: false,
                tags: [Tags.education, Tags.health],
                complete: false,
                deleted: false,
                matchWord: ''
            }
        ] as ITask[]
    }),
    getters: {},
    actions: {
        getMyTasks(): ITask[] {
            return this.tasks.filter(task => !task.deleted && !task.complete)
        },
        getImportance(): ITask[] {
            return this.tasks.filter(task => task.important && !task.complete)
        },
        getCompletes(): ITask[] {
            return this.tasks.filter(task => task.complete)
        },
        deleteTask(id: string) {
            this.tasks = this.tasks.filter((task) => task.id !== id)
        },
    },
    persist: true
})