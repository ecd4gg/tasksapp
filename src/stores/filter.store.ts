import {defineStore} from 'pinia';
import {ITasks} from "../commons/interfaces.ts";
export const useFilterStore = defineStore('FilterStore', {
    state: () => ({
        filters: [] as string[],
        activeFilters: {
            productivity: false,
            education: false,
            health: false,
            urgent: false,
        } as ITasks
    }),
    getters: {},
    actions: {
        async empty(): Promise<void> {
            this.$reset()
        }
    },
    persist: true
})