import {defineStore} from 'pinia';
import {ITask} from "../commons/interfaces.ts";


export const useDeletedTaskStore = defineStore('deletedTaskStore', {
    state: () => ({
        tasks: [
        ] as ITask[]
    }),
    getters: {},
    actions: {
        deleteTask(id: string) {
            this.tasks = this.tasks.filter((task) => task.id !== id)
        },
    },
    persist: true
})