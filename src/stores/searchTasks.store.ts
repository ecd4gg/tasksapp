import {defineStore} from 'pinia';
import {ITask} from "../commons/interfaces.ts";
export const useSearchTasksStore = defineStore('searchTasksStore', {
    state: () => ({
        tasks: [] as ITask[]
    }),
    getters: {},
    actions: {
        setSearchResult(tasks: ITask[], text: string) {
            this.tasks = (tasks.filter(task => {
                if(task.title.indexOf(text) !== -1){
                    task.matchWord = text;
                    return task;
                }
            }))
        },
        async empty(): Promise<void> {
            this.$reset()
        },
    },
    persist: true
})