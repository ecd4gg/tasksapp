import { createApp } from 'vue'
import router from "./router.ts";
import './style.css'
import App from './App.vue'
import pinia from "./stores";

createApp(App).use(router).use(pinia).mount('#app')
